#!/bin/bash
### Author: Nilton OS <jniltinho dot gmail.com>
### Version 0.5

fullfile=${2}
filename=$(basename "$fullfile")
extension="${filename##*.}"
filename="${filename%.*}"

FFMPEG="/usr/bin/ffmpeg"

extract_video_stream() {

  if [ -e "$fullfile" ]; then
      $FFMPEG -y -loglevel panic -hide_banner -i "$fullfile" -c copy -an ${filename}_VS.${extension}
      echo "VIDEO FILE EXTRACT: ${filename}_VS.${extension}"
  else
      echo "VIDEO FILE $fullfile NOT FOUND !!"
  fi
  
}


merger_video_audio() {
  if [ -e "${filename}_VS.${extension}" ] && [ -e "${filename}.wav" ]; then
      $FFMPEG -y -loglevel panic -hide_banner -i ${filename}_VS.${extension} -i ${filename}.wav -c:v copy -c copy ${filename}_VA.${extension}
      echo "VIDEO and AUDIO MERGER: ${filename}_VA.${extension}"
      rm -f "${filename}_VS.${extension}"
  else
      echo "AUDIO FILE ${filename}.flac or VIDEO FILE ${filename}_VS.${extension} NOT FOUND !!"

  fi
}


convert_video_nvenc() {
## Convert video using nvenc and ffmpeg
  if [ -e "$fullfile" ]; then
      $FFMPEG -y -i "$fullfile" -vcodec h264_nvenc -pix_fmt nv12 -level 5.1 \
      -b:v 15M -qmin 18 -qmax 38 -preset llhq -cq 10 -bf 2 -g 150 -profile:v high -delay 0 -acodec copy ${filename}_OK.${extension}
      echo "VIDEO FILE CONVERTED: ${filename}_OK.${extension}"
  else
      echo "VIDEO FILE $fullfile NOT FOUND !!"
  fi
}




case "$1" in
  extract-video)
    extract_video_stream
    ;;
  merger-video)
    extract_video_stream
    merger_video_audio
    ;;
  convert-video)
    convert_video_nvenc
    ;;
  *)
    echo "Usage: $0 {extract-video VIDEO_FILE|merger-video VIDEO_FILE|convert-video VIDEO_FILE}"
esac
