# DEB Package OBS-STUDIO-PLUS for Ubuntu

![obs-studio-banner](obs-studio-banner.jpg)


## Install Ubuntu 18.04 - Bionic

```bash
apt-get update
add-apt-repository ppa:obsproject/obs-studio -y
add-apt-repository ppa:mc3man/bionic-media -y
apt-get update
apt-get -y install libcodec2-0.7 ffmpeg obs-studio

cd /tmp/
wget https://github.com/jniltinho/packages/releases/download/v1.0.0/obs-studio-plus_25.0.8+bionic-1_amd64.deb
dpkg -i obs-studio-plus_*+bionic-1_amd64.deb
```

## Install Ubuntu 20.04 - Eoan

```bash
apt-get update
add-apt-repository ppa:obsproject/obs-studio -y
apt-get update
apt-get -y install libcodec2-0.7 ffmpeg obs-studio

cd /tmp/
wget https://github.com/jniltinho/packages/releases/download/v1.0.0/obs-studio-plus_25.0.8+eoan-1_amd64.deb
dpkg -i obs-studio-plus_*+eoan-1_amd64.deb
```

## Build FFMPEG + OBS Studio Latest + (NVENC|AMFenc)

## Distros Support

* Ubuntu 18.04
* Ubuntu 20.04

## Compile FFMPEG + OBS Studio Latest + (NVENC|AMFenc) in the Docker

```bash
## Run as root (sudo su)
## First you need to install docker.
## sudo apt-get update
## sudo apt-get -y install apt-transport-https ca-certificates wget software-properties-common docker.io socat

mkdir -p install && cd install
wget https://gitlab.com/jniltinho/obs-studio-plus/raw/master/obs-studio-plus_build.sh

## For Ubuntu 18.04
docker run --rm -it -w /install -v "${PWD}:/install" ubuntu:bionic /bin/bash
bash obs-studio-plus_build.sh --dest /opt/obs-studio-plus
cp -aR /root/dist/*.deb /install/
exit
```


### Publish on GitHub

 - https://github.com/cheton/github-release-cli
 - https://github.com/buildkite/github-release
 - https://github.com/obsproject/obs-studio/releases