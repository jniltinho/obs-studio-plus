# Builder image
FROM ubuntu:xenial
LABEL maintainer="Nilton OS <jniltinho@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive


## Links
## https://github.com/cellofellow/ffmpeg/blob/master/Dockerfile
## https://seanthegeek.net/455/how-to-compile-and-install-ffmpeg-4-0-on-debian-ubuntu/
## https://superuser.com/questions/1299064/error-cuvid-requested-but-not-all-dependencies-are-satisfied-cuda-ffnvcodec
## docker run --rm -it -v "${PWD}:/root" ubuntu:xenial /bin/bash


RUN apt-get update \
    && apt-get -y install apt-transport-https \
    ca-certificates curl software-properties-common dpkg-dev


RUN add-apt-repository ppa:jonathonf/ffmpeg-4 -y \
    && sed -i "/^# deb-src/ s/^# //" /etc/apt/sources.list.d/jonathonf-ubuntu-ffmpeg-4-$(lsb_release -cs).list \
    && apt-get update \
    && apt-get -y install build-essential libspeexdsp-dev pkg-config cmake git devscripts lintian checkinstall \
    && apt-get -y build-dep ffmpeg


RUN mkdir -p /install_ffmpeg && cd /install_ffmpeg \
    && apt-get source ffmpeg